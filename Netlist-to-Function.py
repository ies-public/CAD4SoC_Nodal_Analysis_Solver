#This Code reads in the file Netlist.txt and generates a visual representation of the modified nodal analysis matrix and vectors for this netlist.
#This version outputs the results in the terminal. No further librarys must be installed.
#Currently supportet devices are resistors, inductors, capacitors, current sources and voltage sources.
#
#(VW 2023)
		
#Read in From File. If Line is empty skip it! (No further Checks so far)		
def read_netlist_file(filename):
    vector = []
    with open(filename, 'r') as file:
        for line in file:
            stripped_line = line.strip()
            if len(stripped_line) > 0:
                vector.append(stripped_line)
    return vector

netlist_filename = "Netlist"
result_vector = read_netlist_file(netlist_filename)
unknown_values = []
sources = []
for entry in result_vector:        
#Check for unknown voltage potentials and add them to the unknown values vector
    for index, char in enumerate(entry):
        if (index + 1) % 4 == 0 or (index + 1) % 6 == 0:
            if char != "0" and ("u" + char) not in unknown_values:
                unknown_values.append("u" + char)
                sources.append("0")

for entry in result_vector:
#Check for voltage sources and add them to the sources vector and their currents to the unknown values vector
	if entry[0] == "V":
		unknown_values.append("I(" + entry[0] + entry[1] + ")")
		sources.append(entry[0] + entry[1])
		
#Check for current sources and add them to the sources vector       
	if entry[0] == "I":
		if(int(entry[3]) != 0):
			if(sources[int(entry[3])-1] == "0"):		
				sources[int(entry[3])-1] = ("-" + entry[0] + entry[1])
			else:
				sources[int(entry[3])-1] = (sources[int(entry[3])-1] + "-" + entry[0] + entry[1])
		if(int(entry[5]) != 0):
			if(sources[int(entry[5])-1] == "0"):
				sources[int(entry[5])-1] = (entry[0] + entry[1])
			else:
				sources[int(entry[5])-1] = (sources[int(entry[5])-1] + "+" + entry[0] + entry[1])
				
#Check for inductances and add their current to the unknown values vector
for entry in result_vector:
    if entry[0] == "L":
                unknown_values.append("I(" + entry[0] + entry[1] + ")")
                sources.append("0")

matrix = [["0"] * len(unknown_values) for _ in range(len(sources))]
for i in range(len(sources)):
	for j in range(len(unknown_values)):
		for entry_r in result_vector:
#Edit the matrix to include the equations for resistors.
			if(entry_r[0] == "R"):
				if(i==j):	#Main Diagonal					
					if(entry_r[3] != "0" and int(entry_r[3]) == j+1):
						if(matrix[i][j] != "0"):
							matrix[i][j] =  matrix[i][j] + " + 1/R" + entry_r[1]
						else:
							matrix[i][j] = ("1/R" + entry_r[1])
					if(entry_r[5] != "0" and int(entry_r[5]) == j+1):
						if(matrix[i][j] != "0"):
							matrix[i][j] =  matrix[i][j] + " + 1/R" + entry_r[1]
						else:
							matrix[i][j] = ("1/R" + entry_r[1])
				else:		#Sides
					if(entry_r[3] != "0" and int(entry_r[3]) == j+1 and int(entry_r[5]) == i+1):
						if(matrix[i][j] != "0"):
							matrix[i][j] = matrix[i][j] + " - 1/R" +  entry_r[1]
						else:
							matrix[i][j] = ("- 1/R" + entry_r[1])
					if(entry_r[5] != "0" and int(entry_r[5]) == j+1 and int(entry_r[3]) == i+1):
						if(matrix[i][j] != "0"):
							matrix[i][j] = matrix[i][j] + " - 1/R" +  entry_r[1]
						else:
							matrix[i][j] = ("- 1/R" + entry_r[1])
							
#Edit the matrix to include the equations for capacitors.
			if(entry_r[0] == "C"):
					if(i==j):	#Main Diagonal					
						if(entry_r[3] != "0" and int(entry_r[3]) == j+1):
							if(matrix[i][j] != "0"):
								matrix[i][j] =  matrix[i][j] + " + C * d/dt" + entry_r[1]
							else:
								matrix[i][j] = ("C * d/dt" + entry_r[1])
						if(entry_r[5] != "0" and int(entry_r[5]) == j+1):
							if(matrix[i][j] != "0"):
								matrix[i][j] =  matrix[i][j] + " + C * d/dt" + entry_r[1]
							else:
								matrix[i][j] = ("C * d/dt" + entry_r[1])
					else:		#Sides
						if(entry_r[3] != "0" and int(entry_r[3]) == j+1 and int(entry_r[5]) == i+1):
							if(matrix[i][j] != "0"):
								matrix[i][j] = matrix[i][j] + " - C * d/dt" +  entry_r[1]
							else:
								matrix[i][j] = ("- C * d/dt" + entry_r[1])
						if(entry_r[5] != "0" and int(entry_r[5]) == j+1 and int(entry_r[3]) == i+1):
							if(matrix[i][j] != "0"):
								matrix[i][j] = matrix[i][j] + " - C * d/dt" +  entry_r[1]
							else:
								matrix[i][j] = ("- C * d/dt" + entry_r[1])

#Edit the matrix to include the equations for voltage sources.
for entry_r in result_vector:
	if(entry_r[0] == "V"):
		for i in range(len(sources)):
			#print(sources[i][0])
			if(sources[i][0] == "V"):
				if(sources[i][1] == entry_r[1]):
					if(entry_r[3] != "0"):
						#print(entry_r, sources)
						matrix[i][int(entry_r[3])-1] = "1"
						matrix[int(entry_r[3])-1][i] = "-1"
					if(entry_r[5] != "0"):
						#print(entry_r, "Index i:", i)
						matrix[i][int(entry_r[5])-1] = "-1"
						matrix[int(entry_r[5])-1][i] = "1"
						
#Edit the matrix to include the equations for inductances.
	if(entry_r[0] == "L"):
		for i in range(len(unknown_values)):
			if(unknown_values[i][0] == "I"):
				if(unknown_values[i][2] == "L"):
					if(unknown_values[i][3] == entry_r[1]):
						if(entry_r[3] != "0"):
							matrix[i][int(entry_r[3])-1] = "- L * d/dt"
							matrix[int(entry_r[3])-1][i] = "1"
						if(entry_r[5] != "0"):
							matrix[i][int(entry_r[5])-1] = "L * d/dt"
							matrix[int(entry_r[5])-1][i] = "-1"
			
#Plotting of equation system.
print("Sources:")
print(sources)
print("Unknown Values:")
print(unknown_values)
print("Matrix:")
for row in matrix:
    print(row)
