**The objective of this project is to create the equation system for a modified nodal analysis from a netlist.**

Start the python scripts from terminal. There are two scripts. Netlist-to-Function.py outputs the result in the terminal. Netlist-to-Function_graphical outputs a visual figure of the final equation system. For the later one to work you must install the python libraries numpy and matplotlib. You can do this by:

- pip install numpy
- pip install matplotlib

In the file Netlist you can find an example netlist of an example circuit. In the current version you can use resistors, capacitors, inductances current sources and voltage sources. 

*The project is not well tested and there might be bugs! The source code is far from beautiful and should not be used as a reference for coding in python! If you are willing to contribute to this project (e.g. via a seminar) feel free to contact me (Viktor) via viktor.weinelt@ies.tu-darmstadt.de*
